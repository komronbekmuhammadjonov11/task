import './App.css';
import React, {useState} from 'react';

function App() {
    const [travelloList, setTravelloList] = useState([]);
    const [travelloName, setTravelloName] = useState("");
    let messageList = [];

    function addTravello() {
        if (travelloName !== "") {
            let object = {
                id: Date.now(),
                messageList: [],
                name: travelloName
            };
            setTravelloList((prev) => prev.concat(object));
            setTravelloName("")
        }
    }

    function changeMessageList(e, index) {
        messageList[index] = e.target.value;
    }

    function deleteTravelloList(id) {
        let newArray = [];
        newArray = travelloList.filter((item) => {
            return item.id !== id;
        });
        setTravelloList(newArray)
    }

    function addMessageList(id, index) {
        let newArray = [];
        let input = document.querySelectorAll('.input');
        newArray = travelloList.filter((item) => {
            if (id === item.id) {
                return {
                    ...item,
                    messageList: item.messageList.push(messageList[index])
                };
            } else return item;
        });
        input.forEach((item2) => {
            item2.value = "";
        });
        setTravelloList(newArray);
    }

    function position(e) {
        e.target.style.position = 'absolute';
        e.target.style.top = window.clientY + "px";
        e.target.style.left = window.clientX + "px";
    }

    function mouseDown(e) {
        // let a=document.getElementById(`${id}`).children;
        // console.log(a);
        console.log("ewfwefwef")
        console.log(e.target);
        window.addEventListener('mousemove', () => position(e))
    }

    function mouseUp() {

    }

    return (
        <div className="App">
            <div className="row">
                <div className="col-md-3 p-5">
                    <div className="input-group">
                        <button className="btn btn-primary" onClick={addTravello}>Add Travello toy</button>
                        <input type="text" value={travelloName} onChange={(e) => setTravelloName(e.target.value)}
                               className="form-control"/>
                    </div>
                </div>
                {travelloList.map((item, index) => {
                    return <div className="col-md-3 mb-5 mt-2" onMouseUp={mouseUp}>
                        <div className="card pb-3  ">
                            <div className="name p-2 d-flex align-items-center justify-content-between">
                                <div>{item.name}</div>
                                <img src="/images/close.png" onClick={() => deleteTravelloList(item.id)}
                                     alt="no images"/>
                            </div>
                            <div className="input-group p-2">
                                <input type="text" className="form-control input" value={messageList[index]}
                                       onChange={(e) => changeMessageList(e, index)}/>
                                <button className="btn btn-primary" onClick={() => addMessageList(item.id, index)}>Add
                                </button>
                            </div>
                            <ul className={`list-unstyled`} id={item.id}>
                                {item.messageList.map((item2, index2) => {
                                    return <li className={`pl-2 pr-2`} onMouseDown={mouseDown}>{item2}</li>
                                })}
                            </ul>
                        </div>
                    </div>
                })}
            </div>
        </div>
    );
}

export default App;
